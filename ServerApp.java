import java.io.IOException;

public class ServerApp {
	public static void main(String[] args) {
		Server server = new Server(1234);
		try {
			server.listen();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("Server is listening a port " + server.getPort());
	}
}
