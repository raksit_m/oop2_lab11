import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;

public class Server extends AbstractServer {
	private static final String STATE = "State";
	final static int LOGGEDIN = 0;
	final static int LOGGEDOUT = 1;
	final static int SENDING = 2;
	private List<ConnectionToClient> clients = new ArrayList<ConnectionToClient>();
	private List<ConnectionToClient> receivers = new ArrayList<ConnectionToClient>();

	public Server(int port) {
		super(port);
	}

	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		if (!(msg instanceof String)) {
			try {
				client.sendToClient("Unrecognized Message");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		
		String message = (String) msg;
		int state = (Integer) client.getInfo(STATE);

		switch (state) {
		case LOGGEDOUT: {
			if (message.contains("Login")) {
				String username = message.substring(6).trim();
				client.setInfo("User", username);
				client.setInfo(STATE, LOGGEDIN);
				super.sendToAllClients(username + " Connected");
			}

			else {
				try {
					client.sendToClient("Please Login");
					return;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			break;
		}
		case LOGGEDIN: {
			if (message.equalsIgnoreCase("Logout")) {
				client.setInfo(STATE, LOGGEDOUT);
				try {
					client.sendToClient("Goodbye");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (message.contains("To:")) {
				String uc = "";
				String username = message.substring(4).trim();
				for (ConnectionToClient c : clients) {
					if (c.getInfo("User").equals(username)) {
						uc = username;
						client.setInfo(STATE, SENDING);
						receivers.add(c);
					}
				}
			}

			else {
				String username = (String) client.getInfo("User");
				super.sendToAllClients(username + ": " + msg);
			}

			break;
		}
		case SENDING: {
			String username = (String) client.getInfo("User");
			if (message.equalsIgnoreCase("Logout")) {
				client.setInfo(STATE, LOGGEDOUT);
				try {
					super.sendToAllClients(username + " Disconnected");
					client.sendToClient("Goodbye");
					return;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			for (ConnectionToClient r : receivers) {
				try {
					r.sendToClient(username + ": " + message);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}		
		
		}
	}

	public void clientConnected(ConnectionToClient client) {
		clients.add(client);
		client.setInfo(STATE, LOGGEDOUT);
	}

	public synchronized void clientDisconnected(ConnectionToClient client) {
		clients.remove(client);
	}
}