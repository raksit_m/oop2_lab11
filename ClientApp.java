import java.io.IOException;
import java.util.Scanner;

public class ClientApp {

	public static void main(String[] args) {

		Client client = new Client("158.108.225.126", 5555);
		try {
			client.openConnection();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Scanner scanner = new Scanner(System.in);
		while(client.isConnected()) {
			String msg = scanner.nextLine();
			try {
				client.sendToServer(msg);
			} catch (IOException e) {
				e.printStackTrace();
			}

			if(msg.equals("quit"))
				try {
					client.closeConnection();
					break;
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		}
	}
}
